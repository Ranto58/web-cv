<?php
ini_set("display_errors", "1");
session_start();
if(isset($_POST) && isset($_POST['nom'])) {
    $insertSql = 'INSERT INTO `formulaire` (`nom`, `email`, `message`) VALUES ("'.$_POST['nom'].'", "'.$_POST['email'].'", "'.$_POST['message'].'" )';
    $test = "SELECT * FROM formulaire";
    executeSQL($insertSql);

}

function echoAndEmpty($key) {
    if(isset($_SESSION[$key])){
        echo $_SESSION[$key];
        $_SESSION[$key] = "";
    }
    else {
    }
}
function executeSQL($sql) {
    // For debug purpose only
    $user = "antovzvx_Antonio";
    $password = "initialdream";
    $db = "antovzvx_web-cv";
    $host = "localhost";
    $port = 2083;

    $link = mysqli_init();
    $success = mysqli_real_connect(
        $link,
        $host,
        $user,
        $password,
        $db,
        $port
    );
$result = mysqli_query($link, $sql) or die ('Erreur SQL. Detail: '.mysqli_error($link));
    mysqli_close($link);


    $_SESSION["success"] ="<i class=\"far fa-check-circle\"></i> Votre message a été envoyé";
    return $result;
}
// if(isset($_POST['user']) && isset($_POST['mdp'])){
//     $result = $db->query('SELECT * FROM user');
//     while($data=$result->fetch()){
//         if($data['username']==$_POST['user'] && $data['password']==$_POST['mdp']){
//             header('Location:../contact/contact.php');
//         }
//         else{
//             header('Location:../log/login.php');
//         }
//     }
// }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="contact.css">
    <link rel="stylesheet" media="screen and (max-width: 855px)" href="style/contact.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="icon" type="image/jpg" href="assets/img/letter-r.png" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Me</title>
</head>

<body>
    <section id="contact">
        <div class="container">
            <div class="titre">CONTACTEZ-MOI</div>
            <p style="color: #4f4"><?php echoAndEmpty("success"); ?></p>
            <form name="form" method="POST" action="#">
                <div class="name">
                    <input type="text" name="nom" placeholder="NOM" required>
                </div>
                <div class="mail">
                    <input type="email" name="email" placeholder="E-MAIL" required>
                </div>
                <div class="comments">
                    <textarea name="message" placeholder="Laissez un message..." required></textarea>
                </div>
                <div class="bouton">
                    <button name="Envoyer" type="submit" class="submit">Envoyer</button>
                </div>
            </form>
        </div>
    </section>
</body>

</html>
