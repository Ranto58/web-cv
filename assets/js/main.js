menuItems = [];
discoverMenuItems();
scan();

isAnimationsDone = false;
//navbar
window.onscroll = () => {
  const nav = document.querySelector('#container_nav');
  if (this.scrollY <= 500) nav.className = '';
  else nav.className = 'scroll';

  if (this.scrollY > 600 && !isAnimationsDone) {
    isAnimationsDone = true;
    doAnimation();
  }
};  


function discoverMenuItems() {
  let id = 'menuItem';
  let index = 0;
  let item = document.getElementById(`${id}${index}`);
  while (item != null) {
    menuItems.push(item);
    index++;
    item = document.getElementById(`${id}${index}`);
  }
}

function scan() {
  menuItems.forEach(item => {
    item.onclick = function () {
      closeMenu();
    };
  });
}

function doAnimation() {
  //skill bar
  $('.skill-percent').each(function () {
    $(this).animate({
      width: $(this).attr('data-percent')
    }, "fast");
  });
}
$("#menuToggle").on("click", function () {
  if ($("#menu").css("transform") == "none") { // Close le menu
    closeMenu();

  } else {
    openMenu();

  }
})

function openMenu() {
  $("#menu").css("transform", "none");
  $("#menuToggle").addClass("change");
}

function closeMenu() {
  $("#menu").css("transform", "translateX(-100%)")
  $("#menuToggle").removeClass("change");
}